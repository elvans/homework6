import java.util.*;

/* I have used for this work some sources:
 * https://www.geeksforgeeks.org/ford-fulkerson-algorithm-for-maximum-flow-problem/ - here I used the Idea for the code
 * and some code parts.
 * https://www.youtube.com/watch?v=Tl90tNtKvxs - this is the video, which I watched to understand the principles how
 * Ford-Fulkenson algorithm for Max flow works
 */

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

   /** Main method. */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();
   }

   /** Actual main method to run examples and everything. */
   public void run() {
      Graph g = new Graph ("G");
      g.createRandomSimpleGraph (6, 9);
      System.out.println(g.toString());

      int[][] matrix = g.createAdjMatrix();

      System.out.println(g.matrixString(g.createAdjMatrix()));

      Graph hei = g.addRandomFlowsToGraph();
      System.out.println(hei.toString());

      int[][] flowMatrix = hei.createAdjMatrix();

      System.out.println(hei.matrixString(hei.createAdjMatrix()));

      int source = 0;
      int sink = hei.createAdjMatrix().length - 1;
      String sourceSt = "v" + (source + 1);
      String sinkSt = "v" + (sink + 1);

      int flow = hei.fordFulkerson(hei.createAdjMatrix(), source, sink);

      System.out.println("Flow from " + sourceSt + " to " + sinkSt + " is equal to " + flow);

   }

   // Nothing was changed here
   /** Vertex represents one point of the graph
    */
   class Vertex {

      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;

      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      Vertex (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }
   }


   /** Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    * Parameter flow added to the given edge.
    */
   class Arc {

      private String id;
      private Vertex target;
      private Arc next;
      private int flow = 0;
      // You can add more fields, if needed

      Arc (String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      Arc (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      // TODO!!! Your Arc methods here!
   } 

   /** Graph class where are graph creation and changing methods added.
    */
   class Graph {

      private String id;
      private Vertex first;
      private int info = 0;

      Graph (String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph (String s) {
         this (s, null);
      }

      /** Create string from given graph.
       * After name of the graph is added ->
       * write all vertexes on different lines and describe all its edges
       * where given vertex is a starting point.
       * If flow of edge is greater than 0, then this parameter is also added to string.
       * @return String of the graph
       */
      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer (nl);
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append (" ");
               sb.append (a.toString());
               sb.append (" (");
               sb.append (v.toString());
               sb.append ("->");
               sb.append (a.target.toString());
               sb.append (")");
               sb.append(a.flow > 0 ? " flow: " + a.flow : "");
               a = a.next;
               if(a != null){
                   sb.append(",");
               }
            }
            sb.append (nl);
            v = v.next;
         }
         return sb.toString();
      }

       /** Create a new vertex with a given name.
        * @param vid represents the name of a new vertex
        * @return Vertex
        */
      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         return res;
      }

       /** Create a new edge between two points with one direction.
        * @param aid is a name of a new edge
        * @param from is vertex that is a start point
        * @param to is destination vertex
        * @return new edge
        */
      public Arc createArc (String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("a" + varray [vnr].toString() + "_"
                  + varray [i].toString(), varray [vnr], varray [i]);
               createArc ("a" + varray [i].toString() + "_"
                  + varray [vnr].toString(), varray [i], varray [vnr]);
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph.
       * If edge of graph has any flow, then the flow capacity is written in matrix,
       * number 1 and 0 only represent the existence of the edge.
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res [i][j] = a.flow > 0 ? a.flow : 1;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph (int n, int m) {
         if (n <= 0) {
             return;
         }
         if (n > 2500) {
             throw new IllegalArgumentException("Too many vertices: " + n);
         }
         if (m < n-1 || m > n*(n-1)/2) {
             throw new IllegalArgumentException
                     ("Impossible number of edges: " + m);
         }
         first = null;
         createRandomTree (n);       // n-1 edges created here
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            if (i==j) 
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0) 
               continue;  // no multiple edges
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected [i][j] = 1;
            createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected [j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }

      /** Create copy of the main graph with randomly inserted flows with capacities 1-20.
       * Take main graph matrix and change all 1 to capacity value.
       * Create new graph with the edges carrying capacity with them
       * @return graph with already added flows
       */
      public Graph addRandomFlowsToGraph(){
         int[][] matrix = new int[createAdjMatrix().length][createAdjMatrix()[0].length];
         for(int row = 0; row < createAdjMatrix().length; row++){
            for(int col = 0; col < createAdjMatrix()[0].length; col++){
               if(createAdjMatrix()[row][col] == 1 && matrix[row][col] == 0){
                  int flow = (int)(Math.random() * 20);
                  matrix[row][col] = flow; // symmetrically insert flow capacity to new matrix
                  matrix[col][row] = flow;
               }
            }
         }

         Graph graphWithFlows = new Graph(id); // create new graph with same id
         Vertex[] vertexes = new Vertex [info];
         for (int i = info; i > 0; i--){
            graphWithFlows.first = graphWithFlows.createVertex("v" + i); // add all vertexes
            vertexes[i - 1] = graphWithFlows.first; // append vertex list
         }

         for(int row = 0; row < matrix.length; row++){
            for(int col = 0; col < matrix[0].length; col++){
               if(matrix[row][col] != 0){
                  int flow = matrix[row][col]; // take flow from matrix
                  Arc a = graphWithFlows.createArc("a" + vertexes[row] + "_" + vertexes[col], vertexes[row], vertexes[col]); // create new edge
                  a.flow = flow; // add flow
               }
            }
         }
         return graphWithFlows; // return new graph based on main graph with flows
      }


      /** Fills parent[] to store the path, that was already found
       * @return true if path from source to sink exist
       */
      public boolean bfs(int gMatrix[][], int source, int sink, int parent[])
      {
         boolean[] visited = new boolean[gMatrix.length]; // visited vertexes' list
         for(int i=0; i<gMatrix.length; ++i) {
            visited[i] = false; // mark all vertexes as not visited
         }

         LinkedList<Integer> queue = new LinkedList<>(); // create queue list to
         queue.add(source); // add source to queue
         visited[source] = true; // mark as visited
         parent[source]=-1;

         // If there are still some vertexes that are not looked through - continue
         while (queue.size()!=0)
         {
            int u = queue.poll(); // takes first element from list queue

            for (int v=0; v<gMatrix.length; v++)
            {
               if (!visited[v] && gMatrix[u][v] > 0) // if vertex wasn't visited and according to the matrix way exist
               {
                  queue.add(v);                      // add all possible vertexes, which may be path to sink, to the queue
                  parent[v] = u;                     // add them to the parent list
                  visited[v] = true;                 // mark as visited
               }
            }
         }

         return visited[sink]; // If sink was reached, method returns 'true'
      }

      /** Returns tne maximum flow from source to sink in the given graph using Ford-Fulkerson algorithm.
       * @param gMatrix is a matrix with marked flows on it
       * @param source is a source from where start counting flow
       * @param sink is a point to where flow should go
       * @return maximum flow between source and sink vertexes
       */
      public int fordFulkerson(int gMatrix[][], int source, int sink)
      {
         if(gMatrix.length == 0){ // check if gMatrix is not empty
            throw new RuntimeException("Graph is empty, cannot count flow!");
         }
         if(gMatrix.length <= sink){ // check if source is in bounds
            throw new RuntimeException("Destination vertex v" + (sink + 1) +
                    " is out of bounds in graph with given matrix:\n" + matrixString(gMatrix));
         }
         int u, v;

         int rGraph[][] = new int[gMatrix.length][gMatrix.length];

         for (u = 0; u < gMatrix.length; u++) { // creates copy of given graph's matrix to implement changes
            // to it and to not touch the main graph's matrix
            for (v = 0; v < gMatrix.length; v++) {
               rGraph[u][v] = gMatrix[u][v];
            }
         }

         int parent[] = new int[gMatrix.length]; // This array is filled by BFS method and stores path

         int max_flow = 0;  // There is no flow initially

          if(!bfs(rGraph, source, sink, parent)) { // this is the method, that checks if path exists and throws exception if don't
              throw new RuntimeException("There is no path from v" + (source + 1) + " to v" + (sink + 1) +
                      " in graph with given matrix:\n" + matrixString(gMatrix));
          }

         while (bfs(rGraph, source, sink, parent)) // Count the flow while there is path from source to sink
         // If path was used - the capacities og edges are decreased
         {
            int path_flow = Integer.MAX_VALUE; // maximum possible flow through the path found
            for (v=sink; v!=source; v=parent[v]) {
               u = parent[v]; // path element
               path_flow = Math.min(path_flow, rGraph[u][v]); // Check if current edge has lower capacity than path,
               // if if is, then current path flow is decreased
            }

            for (v=sink; v != source; v=parent[v]) { // update capacities of the edges on path
               u = parent[v];
               rGraph[u][v] -= path_flow; // decrease the capacity of the path edge
               rGraph[v][u] += path_flow; // increase the capacity of the opposite to path edge
            }

            max_flow += path_flow; // Add path flow to overall flow
         }

         // Return the overall flow
         return max_flow;
      }

      /** Method takes in matrix and returns its string.
       * Method is used to reduce the number of for-loops in methods that print anything out.
       * @param gMatrix is graph's matrix
       * @return string of matrix
       */
      public String matrixString (int[][] gMatrix){
         StringBuilder matrix = new StringBuilder(); // StringBuilder of the given matrix (for the exceptions)
         for (int[] row : gMatrix) {
            matrix.append("[");
            for(int col = 0; col < gMatrix.length; col++){
               String value = row[col] >= 10 ? "" + row[col] : " " + row[col];
               matrix.append(value);
               if(col != gMatrix.length - 1){
                  matrix.append(",");
               }
            }
            matrix.append("]\n");
         }
         return matrix.toString();
      }
   }
} 

