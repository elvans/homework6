import static org.junit.Assert.*;
import org.junit.Test;

/** Testklass.
 * @author jaanus
 */
public class GraphTaskTest {

   @Test (timeout = 1000)
   public void testFordFulkensonWithSmallGraph(){
      GraphTask graphTask = new GraphTask();
      GraphTask.Graph graph = graphTask.new Graph("Graph");
      graph.createRandomSimpleGraph(2, 1);
      int flow = graph.fordFulkerson(graph.createAdjMatrix(), 0, graph.createAdjMatrix().length - 1);
      assertEquals("Flow sould be 1", 1, flow);
   }

    @Test (timeout = 1000)
    public void testFordFulkensonWithOnePath(){
        GraphTask graphTask = new GraphTask();
        GraphTask.Graph graph = graphTask.new Graph("Graph");
        GraphTask.Vertex[] varray = new GraphTask.Vertex[4];
        for (int i = 0; i < 4; i++) {
            varray [i] = graph.createVertex ("v" + String.valueOf(4-i));
            if (i > 0) {
                int vnr = (int)(Math.random()*i);
                graph.createArc ("a" + varray [vnr].toString() + "_"
                        + varray [i].toString(), varray [vnr], varray [i]);
                graph.createArc ("a" + varray [i].toString() + "_"
                        + varray [vnr].toString(), varray [i], varray [vnr]);
            } else {}
        }
        int flow = graph.fordFulkerson(graph.createAdjMatrix(), 0, graph.createAdjMatrix().length - 1);
        assertEquals("Flow should be 1", 1, flow);
    }

    @Test (expected=RuntimeException.class)
    public void testFordFulkensonWithNoPath(){
        GraphTask graphTask = new GraphTask();
        GraphTask.Graph graph = graphTask.new Graph("Graph");
        GraphTask.Vertex[] varray = new GraphTask.Vertex[4];
        for (int i = 0; i < 4; i++) {
            varray [i] = graph.createVertex ("v" + String.valueOf(4-i));
            if (i > 0 && i < 3) {
                int vnr = (int)(Math.random()*i);
                graph.createArc ("a" + varray [vnr].toString() + "_"
                        + varray [i].toString(), varray [vnr], varray [i]);
                graph.createArc ("a" + varray [i].toString() + "_"
                        + varray [vnr].toString(), varray [i], varray [vnr]);
            } else {}
        }
        int flow = graph.fordFulkerson(graph.createAdjMatrix(), 0, graph.createAdjMatrix().length - 1);
        assertEquals("Flow should be 0", 0,flow);
    }

    @Test (expected=RuntimeException.class)
    public void testFordFulkensonEmpty(){
        GraphTask graphTask = new GraphTask();
        GraphTask.Graph graph = graphTask.new Graph("Graph");
        graph.fordFulkerson(graph.createAdjMatrix(), 0, 1);
    }

    @Test (expected = RuntimeException.class)
    public void testFordFulkensonIndexOutOfBound(){
        GraphTask graphTask = new GraphTask();
        GraphTask.Graph graph = graphTask.new Graph("Graph");
        GraphTask.Vertex[] varray = new GraphTask.Vertex[5];
        for (int i = 0; i < 5; i++) {
            varray [i] = graph.createVertex ("v" + String.valueOf(4-i));
            if (i > 0) {
                int vnr = (int)(Math.random()*i);
                graph.createArc ("a" + varray [vnr].toString() + "_"
                        + varray [i].toString(), varray [vnr], varray [i]);
                graph.createArc ("a" + varray [i].toString() + "_"
                        + varray [vnr].toString(), varray [i], varray [vnr]);
            } else {}
        }
        graph.fordFulkerson(graph.createAdjMatrix(), 0, 9);
    }

    @Test (timeout = 1000)
    public void testAddedFlowsDontChangeEdgesLocation(){
        GraphTask graphTask = new GraphTask();
        GraphTask.Graph graph = graphTask.new Graph("Graph");

        GraphTask.Vertex v3 = graph.createVertex("v3");
        GraphTask.Vertex v2 = graph.createVertex("v2");
        GraphTask.Vertex v1 = graph.createVertex("v1");

        graph.createArc("v1v2", v1, v2);
        graph.createArc("v2v1", v2, v1);
        graph.createArc("v2v3", v2, v3);
        graph.createArc("v3v2", v3, v2);

        GraphTask.Graph graph1;
        graph1 = graph.addRandomFlowsToGraph();

        int[][] matrix = graph.createAdjMatrix();
        int[][] matrix1 = graph1.createAdjMatrix();

        System.out.println(graph.matrixString(matrix) + "\n \n");
        System.out.println(graph1.matrixString(matrix1));
        int notEqual = 0;
        for(int row = 0; row < matrix.length; row++){
            for (int col = 0; col < matrix.length; col++){
                if(matrix[row][col] == 0 && matrix1[row][col] != 0){
                    notEqual++;
                }
            }
        }
        assertEquals("Number of non-equal squares: ",0, notEqual);
    }


}

